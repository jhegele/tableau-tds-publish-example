# Tableau TDS Publish Example

This repo demonstrates how to write a TDS file and publish it to Tableau Server using Python. The TDS here is intended to reference a database rather than a local file (CSV, Excel, etc).

## Additional Resources and Documentation

* [Tableau REST API Documentation](https://help.tableau.com/current/api/rest_api/en-us/REST/rest_api_ref.htm#publish_data_source) - Tableau's official documentation for the Publish Data Source method on the REST API.
* [Tableau Document API](https://github.com/tableau/document-api-python) - I based most of the code used to write the TDS on code from this lib. While you can use the Document API, beware that the list of valid dbclass values is not complete/comprehensive (Maria DB is missing, e.g.).
* [Tableau REST API Publish Workbook Sample](https://github.com/tableau/rest-api-samples/blob/master/python/publish_workbook.py) - The code to manage and publish process is largely based on this example. While this specific example demonstrates publishing a workbook, the process is sufficiently similar to publishing a data source that much of the code could be repurposed with minimal changes.

## Authentication Info

This example uses local authentication for both the DB and Tableau Server. If you use a different authentication mechanism (SAML, Active Directory, etc) you'll have to make some adjustments.

Also note that this example intentionally embeds credentials for the data source. If you want to avoid having credentials embedded, you'll need to modify this code.

## Known Limitations

* Because this example publishes a minimally viable TDS, any data source published using this code **will not work with Ask Data**.